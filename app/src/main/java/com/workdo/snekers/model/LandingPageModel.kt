package com.workdo.snekers.model

import com.google.gson.annotations.SerializedName

data class LandingPageModel(

	@field:SerializedName("data")
	val data: LandingPageData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)
data class HomeBestsellers(

	@field:SerializedName("home-bestsellers-text")
	val homeBestsellersText: String? = null,

	@field:SerializedName("home-bestsellers-title")
	val homeBestsellersTitle: String? = null,

	@field:SerializedName("home-bestsellers-button")
	val homeBestsellersButton: String? = null
)

data class HomeTrending(

	@field:SerializedName("home-trending-button")
	val homeTrendingButton: String? = null,

	@field:SerializedName("home-trending-sub-text")
	val homeTrendingSubText: String? = null,

	@field:SerializedName("home-trending-sub-title")
	val homeTrendingSubTitle: String? = null,

	@field:SerializedName("home-trending-title")
	val homeTrendingTitle: String? = null
)

data class HomeHeader(

	@field:SerializedName("home-header-bg-image")
	val homeHeaderBgImage: String? = null,

	@field:SerializedName("home-header-title-text")
	val homeHeaderTitleText: String? = null,

	@field:SerializedName("home-header-sub-text")
	val homeHeaderSubText: String? = null
)

data class LandingPageData(

	@field:SerializedName("them_json")
	val themJson: ThemJson? = null,

	@field:SerializedName("message")
	val message: String? = null,


	@field:SerializedName("loyality_section")
	val loyalitySection: String? = null
)

data class HomeCategories(

	@field:SerializedName("home-categories-button")
	val homeCategoriesButton: String? = null,

	@field:SerializedName("home-categories-text")
	val homeCategoriesText: String? = null,

	@field:SerializedName("home-categories-title")
	val homeCategoriesTitle: String? = null
)

data class HomeLoyaltyProgram(

	@field:SerializedName("home-loyalty-program-sub-text")
	val homeLoyaltyProgramSubText: String? = null,

	@field:SerializedName("home-loyalty-program-text")
	val homeLoyaltyProgramText: String? = null,

	@field:SerializedName("home-loyalty-program-bg-image")
	val homeLoyaltyProgramBgImage: String? = null,

	@field:SerializedName("home-loyalty-program-title")
	val homeLoyaltyProgramTitle: String? = null,

	@field:SerializedName("home-loyalty-program-button")
	val homeLoyaltyProgramButton: String? = null
)

data class ThemJson(

	@field:SerializedName("home-categories")
	val homeCategories: HomeCategories? = null,

	@field:SerializedName("home-loyalty-program")
	val homeLoyaltyProgram: HomeLoyaltyProgram? = null,

	@field:SerializedName("home-featured-product")
	val homeFeaturedProduct: HomeFeaturedProduct? = null,

	@field:SerializedName("home-trending")
	val homeTrending: HomeTrending? = null,

	@field:SerializedName("home-bestsellers")
	val homeBestsellers: HomeBestsellers? = null,

	@field:SerializedName("home-header")
	val homeHeader: HomeHeader? = null
)

data class HomeFeaturedProduct(

	@field:SerializedName("home-featured-product-title")
	val homeFeaturedProductTitle: String? = null,

	@field:SerializedName("home-featured-product-text")
	val homeFeaturedProductText: String? = null
)

