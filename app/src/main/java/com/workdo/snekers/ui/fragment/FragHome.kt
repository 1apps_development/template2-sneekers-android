package com.workdo.snekers.ui.fragment

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.workdo.snekers.R
import com.workdo.snekers.adapter.*
import com.workdo.snekers.api.ApiClient
import com.workdo.snekers.base.BaseFragment
import com.workdo.snekers.databinding.DlgConfirmBinding
import com.workdo.snekers.databinding.FragHomeBinding
import com.workdo.snekers.model.*
import com.workdo.snekers.remote.NetworkResponse
import com.workdo.snekers.ui.activity.*
import com.workdo.snekers.ui.authentication.ActWelCome
import com.workdo.snekers.ui.option.ActMenu
import com.workdo.snekers.ui.option.ActSearch
import com.workdo.snekers.utils.Constants
import com.workdo.snekers.utils.ExtensionFunctions.hide
import com.workdo.snekers.utils.ExtensionFunctions.show
import com.workdo.snekers.utils.PaginationScrollListener
import com.workdo.snekers.utils.SharePreference
import com.workdo.snekers.utils.Utils
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.lang.reflect.Type

class FragHome : BaseFragment<FragHomeBinding>() {

    private lateinit var _binding: FragHomeBinding

    private var featuredProductsList = ArrayList<FeaturedProducts>()
    private lateinit var categoriesAdapter: CategoriesAdapter
    var subcategory_id: String = ""
    var maincategory_id: String = ""
    private var featuredProductsSubList = ArrayList<FeaturedProductsSub>()
    private lateinit var categoriesProductAdapter: FeaturedAdapter
    private var manager: GridLayoutManager? = null

    internal var isLoading = false
    internal var isLastPage = false
    private var currentPage = 1
    private var total_pages: Int = 0
    var count = 1

    private var homeCategoriesList = ArrayList<HomeCategoriesItem>()
    private var managerAllCategories: GridLayoutManager? = null
    private lateinit var allCategoriesAdapter: AllCateAdapter

    internal var isLoadingCategorirs = false
    internal var isLastPageCategorirs = false
    private var currentPageCategorirs = 1
    private var total_pagesCategorirs: Int = 0

    private var managerBestsellers: GridLayoutManager? = null
    private var bestsellersList = ArrayList<FeaturedProductsSub>()
    private lateinit var bestSellersAdapter: BestsellerAdapter

    private var managerTrandingProduct: GridLayoutManager? = null
    private var trendingProductsSubList = ArrayList<FeaturedProductsSub>()
    private lateinit var trandingAdapter: TrandingProductAdapter

    internal var isLoadingTrending = false
    internal var isLastPageTrending = false
    private var currentPageTrending = 1
    private var total_pagesTrending: Int = 0

    internal var isLoadingBestSeller = false
    internal var isLastPageBestSeller = false
    private var currentPageBestSeller = 1
    private var total_pagesBestSeller: Int = 0

    override fun initView(view: View) {
        init()
    }

    override fun getBinding(): FragHomeBinding {
        _binding = FragHomeBinding.inflate(layoutInflater)
        return _binding
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun init() {

        _binding.clSearch.setOnClickListener { openActivity(ActSearch::class.java) }
        _binding.edSearch.setOnClickListener { openActivity(ActSearch::class.java) }

        _binding.ivMenu.setOnClickListener { openActivity(ActMenu::class.java) }
        _binding.btnShowMoreCategories.setOnClickListener {
            val intent = Intent(requireActivity(), MainActivity::class.java)
            intent.putExtra("pos", "3")
            startActivity(intent)
        }
        _binding.btnShowMorebestsellers.setOnClickListener {
            val intent = Intent(requireActivity(), ActBestsellers::class.java)
            startActivity(intent)
        }
        _binding.ivCart.setOnClickListener { openActivity(ActShoppingCart::class.java) }

        featuredProductsAdapter()
        manager = GridLayoutManager(requireActivity(), 1, GridLayoutManager.HORIZONTAL, false)
        managerAllCategories =
            GridLayoutManager(requireActivity(), 1, GridLayoutManager.VERTICAL, false)
        managerBestsellers =
            GridLayoutManager(requireActivity(), 1, GridLayoutManager.HORIZONTAL, false)
        managerTrandingProduct =
            GridLayoutManager(requireActivity(), 1, GridLayoutManager.HORIZONTAL, false)

        pagination()
        paginationBestSeller()
        paginationTrending()
        /*_binding.btnShowMoreCategories.setOnClickListener {
            isLoadingCategorirs = true
            currentPageCategorirs++
            callCategories()
            //  paginationCategories()
        }*/
        _binding.btnShowMoreBestSellers.setOnClickListener {
            Log.e("ClickBestseller", "bestsellerList")
            isLoadingBestSeller = true
            currentPageBestSeller++
            callBestseller()
            // paginationBestSeller()
        }

        if (SharePreference.getStringPref(requireActivity(), SharePreference.cartCount)
                .isNullOrEmpty()
        ) {
            SharePreference.setStringPref(requireActivity(), SharePreference.cartCount, "0")
        }

        _binding.btnAboutProgram.setOnClickListener {
            if (SharePreference.getBooleanPref(
                    requireActivity(),
                    SharePreference.isLogin
                )
            ) {

                openActivity(ActLoyality::class.java)

            } else {
                Utils.setInvalidToekn(requireActivity())
            }
        }

    }


    private fun paginationBestSeller() {
        val paginationListener = object : PaginationScrollListener(managerBestsellers) {
            override fun isLastPage(): Boolean {
                return isLastPageBestSeller
            }

            override fun isLoading(): Boolean {
                return isLoadingBestSeller
            }

            override fun loadMoreItems() {
                isLoadingBestSeller = true
                currentPageBestSeller++
                callBestseller()
            }
        }
        _binding.rvBestsellers.addOnScrollListener(paginationListener)
    }

    //TODO Currency Api calling
    private fun callCurrencyApi() {
        Utils.showLoadingProgress(requireActivity())
        val currencyMap = HashMap<String, String>()
        currencyMap["theme_id"] =getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setcurrency(currencyMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val currencyResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            SharePreference.setStringPref(
                                requireActivity(),
                                SharePreference.currency,
                                currencyResponse?.currency.toString()
                            )
                            SharePreference.setStringPref(
                                requireActivity(),
                                SharePreference.currency_name,
                                currencyResponse?.currency_name.toString()
                            )
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                currencyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.openWelcomeScreen(requireActivity())
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO Header content api calling
    private fun callHeaderContentApi() {
        Utils.showLoadingProgress(requireActivity())
        val theme = HashMap<String, String>()
        theme["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setLandingPage(theme)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val headerContenResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            //home-header
                            _binding.tvTrending.text =
                                headerContenResponse?.themJson?.homeHeader?.homeHeaderTitleText
                            _binding.tvDesc.text =
                                headerContenResponse?.themJson?.homeHeader?.homeHeaderSubText
                            Glide.with(requireActivity())
                                .load(ApiClient.ImageURL.BASE_URL.plus(headerContenResponse?.themJson?.homeHeader?.homeHeaderBgImage))
                                .into(_binding.ivFirstHome)


                            _binding.tvCollections.text =
                                headerContenResponse?.themJson?.homeFeaturedProduct?.homeFeaturedProductText
                            _binding.tvFeaturedproducts.text =
                                headerContenResponse?.themJson?.homeFeaturedProduct?.homeFeaturedProductTitle

                            _binding.tvFeaturedCollections.text =
                                headerContenResponse?.themJson?.homeCategories?.homeCategoriesText
                            _binding.tvCategories.text =
                                headerContenResponse?.themJson?.homeCategories?.homeCategoriesTitle

                            _binding.btnShowMoreCategories.text =
                                headerContenResponse?.themJson?.homeCategories?.homeCategoriesButton

                            _binding.tvCollectionss.text =
                                headerContenResponse?.themJson?.homeBestsellers?.homeBestsellersText
                            _binding.tvBestsellers.text =
                                headerContenResponse?.themJson?.homeBestsellers?.homeBestsellersTitle
                            _binding.btnShowMorebestsellers.text =
                                headerContenResponse?.themJson?.homeBestsellers?.homeBestsellersButton



                            if (headerContenResponse?.loyalitySection == "on") {
                                _binding.tvProgram.show()
                                _binding.tvProgramDesc.show()
                                _binding.tvProdesc.show()
                                _binding.btnAboutProgram.show()
                                _binding.clLoyality.show()

                                //home-footer
                                _binding.tvProgram.text =
                                    headerContenResponse.themJson?.homeLoyaltyProgram?.homeLoyaltyProgramText
                                _binding.tvProgramDesc.text =
                                    headerContenResponse.themJson?.homeLoyaltyProgram?.homeLoyaltyProgramTitle
                                _binding.tvProdesc.text =
                                    headerContenResponse.themJson?.homeLoyaltyProgram?.homeLoyaltyProgramSubText
                                _binding.btnAboutProgram.text =
                                    headerContenResponse.themJson?.homeLoyaltyProgram?.homeLoyaltyProgramButton
                                Glide.with(requireActivity())
                                    .load(ApiClient.ImageURL.BASE_URL.plus(headerContenResponse.themJson?.homeLoyaltyProgram?.homeLoyaltyProgramBgImage))
                                    .into(_binding.ivProgram)
                            } else {
                                _binding.tvProgram.hide()
                                _binding.tvProgramDesc.hide()
                                _binding.tvProdesc.hide()
                                _binding.btnAboutProgram.hide()
                                _binding.clLoyality.hide()
                            }

                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                headerContenResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                headerContenResponse?.message.toString()
                            )
                            Utils.openWelcomeScreen(requireActivity())

                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //Featured Product Api calling
    private fun callFeaturedProduct() {
        Utils.showLoadingProgress(requireActivity())
        val theme = HashMap<String, String>()
        theme["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {

            val response =
                if (SharePreference.getStringPref(requireActivity(), SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(requireActivity())
                        .setFeaturedProductsGuests(theme)
                }else
                {
                    ApiClient.getClient(requireActivity())
                        .setFeaturedProducts(theme)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val featuredProductResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            featuredProductsList.add(
                                FeaturedProducts(
                                    true,
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "All Products",
                                    "", "", 0, 0
                                )
                            )
                            response.body.data?.let { featuredProductsList.addAll(it) }
                            if (featuredProductsList.size == 0) {
                                _binding.rvCategories.hide()

                            } else {
                                _binding.rvCategories.show()

                            }
                            categoriesAdapter.notifyDataSetChanged()
                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                featuredProductResponse?.get(0)?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                featuredProductResponse?.get(0)?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    // adapter set Featured Product
    private fun featuredProductsAdapter() {
        _binding.rvCategories.layoutManager =
            GridLayoutManager(requireActivity(), 1, GridLayoutManager.HORIZONTAL, false)
        categoriesAdapter =
            CategoriesAdapter(
                requireActivity(),
                featuredProductsList
            ) { id: String, name: String, mainId: String ->
                Log.e("CategoriesName", name)
                Log.e("CategoriesName", id)
                Log.e("CategoriesName", mainId)
                subcategory_id = id.toString()
                maincategory_id = mainId.toString()
                featuredProductsSubList.clear()
                currentPage = 1
                total_pages = 0
                isLastPage = false
                isLoading = false
                callCategorysProduct()
            }
        _binding.rvCategories.adapter = categoriesAdapter
    }

    //Categories Product Api calling
    private fun callCategorysProduct() {
        Utils.showLoadingProgress(requireActivity())
        val categoriesProduct = HashMap<String, String>()
        categoriesProduct["subcategory_id"] = subcategory_id
        categoriesProduct["maincategory_id"] = maincategory_id
        categoriesProduct["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            val response =
                if (SharePreference.getStringPref(requireActivity(), SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(requireActivity())
                        .setCategorysProductGuest(currentPage.toString(), categoriesProduct)
                } else {
                    ApiClient.getClient(requireActivity())
                        .setCategorysProduct(currentPage.toString(), categoriesProduct)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val categoriesProductResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvFeaturedProduct.show()
                                this@FragHome.currentPage =
                                    categoriesProductResponse?.currentPage!!.toInt()
                                this@FragHome.total_pages =
                                    categoriesProductResponse.lastPage!!.toInt()
                                categoriesProductResponse.data?.let {
                                    featuredProductsSubList.addAll(it)
                                }
                                if (currentPage >= total_pages) {
                                    isLastPage = true
                                }
                                isLoading = false
                            } else {
                                _binding.rvFeaturedProduct.hide()
                            }
                            categoriesProductAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesProductResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesProductResponse?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //Categories Product pagination
    private fun pagination() {
        val paginationListener = object : PaginationScrollListener(manager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                currentPage++
                callCategorysProduct()
            }
        }
        _binding.rvFeaturedProduct.addOnScrollListener(paginationListener)
    }

    //Adapter set Categories Product
    private fun categoriesProductsAdapter(featuredProductsSubList: ArrayList<FeaturedProductsSub>) {
        _binding.rvFeaturedProduct.layoutManager = manager
        categoriesProductAdapter =
            FeaturedAdapter(requireActivity(), featuredProductsSubList) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            requireActivity(),
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (featuredProductsSubList[i].inWhishlist == false) {
                            callWishlist(
                                "add",
                                featuredProductsSubList[i].id,
                                "CategoriesProduct",
                                i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                featuredProductsSubList[i].id,
                                "CategoriesProduct", i
                            )
                        }
                    } else {
                     Utils.openWelcomeScreen(requireActivity())

                    }
                } else if (s == Constants.CartClick) {
                    val data = featuredProductsSubList[i]
                    if (!Utils.isLogin(requireActivity())) {
                        guestUserAddToCart(data, featuredProductsSubList[i].id)
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                                .toString()
                        addtocart["product_id"] = featuredProductsSubList[i].id.toString()
                        addtocart["variant_id"] =
                            featuredProductsSubList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = getString(R.string.theme_id)
                        addtocartApi(
                            addtocart,
                            featuredProductsSubList[i].id,
                            featuredProductsSubList[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(requireActivity(), ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, featuredProductsSubList[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvFeaturedProduct.adapter = categoriesProductAdapter
    }

    private fun callWishlist(type: String, id: Int?, itemType: String, position: Int) {
        Utils.showLoadingProgress(requireActivity())
        val wishlist = HashMap<String, String>()
        wishlist["user_id"] =
            SharePreference.getStringPref(requireActivity(), SharePreference.userId).toString()
        wishlist["product_id"] = id.toString()
        wishlist["wishlist_type"] = type
        wishlist["theme_id"]=getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setWishlist(wishlist)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val wishlistResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if (type == "add") {
                                when (itemType) {
                                    "CategoriesProduct" -> {
                                        featuredProductsSubList[position].inWhishlist = true
                                        categoriesProductAdapter.notifyItemChanged(position)
                                    }
                                    "BestsellersList" -> {
                                        bestsellersList[position].inWhishlist = true
                                        bestSellersAdapter.notifyItemChanged(position)
                                    }
                                    "TrendingList" -> {
                                        trendingProductsSubList[position].inWhishlist = true
                                        trandingAdapter.notifyItemChanged(position)
                                    }
                                }
                            } else {
                                when (itemType) {
                                    "CategoriesProduct" -> {
                                        featuredProductsSubList[position].inWhishlist = false
                                        categoriesProductAdapter.notifyItemChanged(position)
                                    }
                                    "BestsellersList" -> {
                                        bestsellersList[position].inWhishlist = false
                                        bestSellersAdapter.notifyItemChanged(position)
                                    }
                                    "TrendingList" -> {
                                        trendingProductsSubList[position].inWhishlist = false
                                        trandingAdapter.notifyItemChanged(position)
                                    }
                                }
                            }
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                wishlistResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                wishlistResponse?.message.toString()
                            )
                            Utils.openWelcomeScreen(requireActivity())

                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }


    private fun guestUserAddToCart(data: FeaturedProductsSub, id: Int?) {
        Log.e("Position", id.toString())
        val cartData = cartData(data)
        Log.e("cartData2", Gson().toJson(cartData))

        val cartListData = SharePreference.getStringPref(
            requireActivity(),
            SharePreference.GuestCartList
        )
        val type: Type = object : TypeToken<ArrayList<ProductListItem>>() {}.type

        var cartList =
            Gson().fromJson<ArrayList<ProductListItem>>(cartListData, type)
        Log.e("cartListBefore2", Gson().toJson(cartList))

        if (cartList == null) {
            //  dlgConfirm(data.name+" "+R.string.addsuccess)
            val cartDataList = ArrayList<ProductListItem>()
            cartDataList.add(cartData)

            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.GuestCartList,
                Gson().toJson(cartDataList)
            )
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.cartCount,
                cartDataList.size.toString()
            )
            _binding.tvCount.text = cartDataList.size.toString()
        } else if (isProductAlreadyAdded(cartList, data.id.toString())) {
            // Utils.errorAlert(requireActivity(), "item already added into cart")
            for (i in 0 until cartList.size) {
                if (cartList[i].productId?.toInt() == id) {
                    val position = cartList[i]
                    Log.e("SelectId", position.toString())
                    dlgAlreadyCart(data.message, data.id, data.defaultVariantId, i)
                }
            }
        } else {
            dlgConfirm(data.name.plus(" ").plus("add successfully."))
            cartList.add(cartData)
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.GuestCartList,
                Gson().toJson(cartList)
            )
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.cartCount,
                cartList.size.toString()
            )
            _binding.tvCount.text = cartList.size.toString()
        }
    }

    private fun isProductAlreadyAdded(cartList: ArrayList<ProductListItem>, id: String): Boolean {
        return cartList.filter { it.productId == id.toString() }.size == 1
    }

    private fun cartData(data: FeaturedProductsSub): ProductListItem {
        return ProductListItem(
            data.coverImagePath.toString(),
            data.defaultVariantId.toString(),
            data.finalPrice.toString(),
            data.discountPrice.toString(),
            data.id.toString(),
            1,
            data.name.toString(),
            "",
            "",
            data.variantName.toString(),
            data.originalPrice.toString()
        )
    }

    private fun addtocartApi(
        addtocart: HashMap<String, String>,
        id: Int?,
        defaultVariantId: Int?,
        i: Int?
    ) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .addtocart(addtocart)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addtocart = response.body.data
                    when (response.body.status) {
                        1 -> {
                            dlgConfirm(response.body.data?.message)
                            SharePreference.setStringPref(
                                requireActivity(),
                                SharePreference.cartCount,
                                addtocart?.count.toString()
                            )
                            setCount(addtocart?.count)
                        }
                        0 -> {
                            dlgAlreadyCart(response.body.data?.message, id, defaultVariantId, i)

                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                addtocart?.message.toString()
                            )
                            Utils.openWelcomeScreen(requireActivity())

                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun setCount(count: Int?) {
        _binding.tvCount.text = count.toString()
    }

    private fun dlgConfirm(message: String?) {
        val confirmDialogBinding = DlgConfirmBinding.inflate(layoutInflater)
        val dialog = Dialog(requireActivity())
        dialog.setContentView(confirmDialogBinding.root)
        confirmDialogBinding.tvProductDesc.text = message
        confirmDialogBinding.tvCheckOut.setOnClickListener {
            dialog.dismiss()
            openActivity(ActShoppingCart::class.java)
        }
        confirmDialogBinding.tvContinueShopping.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun dlgAlreadyCart(message: String?, id: Int?, defaultVariantId: Int?, i: Int?) {
        val builder = AlertDialog.Builder(requireActivity())
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.product_already_again)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            val cartqty = java.util.HashMap<String, String>()

            if (Utils.isLogin(requireActivity())) {
                count = 1
                cartqty["product_id"] = id.toString()
                cartqty["user_id"] =
                    SharePreference.getStringPref(
                        requireActivity(),
                        SharePreference.userId
                    ).toString()
                cartqty["variant_id"] = defaultVariantId.toString()
                cartqty["quantity_type"] = "increase"
                cartqty["theme_id"]=getString(R.string.theme_id)
                cartQtyApi(cartqty, "increase")
            } else {
                val cartListData = SharePreference.getStringPref(
                    requireActivity(),
                    SharePreference.GuestCartList
                )
                val type: Type = object : TypeToken<java.util.ArrayList<ProductListItem>>() {}.type

                var cartList =
                    Gson().fromJson<java.util.ArrayList<ProductListItem>>(cartListData, type)
                var count = cartList[i!!].qty?.toInt()!!
                Log.e("Count", count.toString())
                cartList[i!!].qty = count.plus(1)

                manageOfflineData(cartList)
            }
        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            dialogInterface.dismiss()
            builder.setCancelable(true)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    private fun manageOfflineData(cartItemList: java.util.ArrayList<ProductListItem>) {

        SharePreference.setStringPref(
            requireActivity(),
            SharePreference.GuestCartList,
            Gson().toJson(cartItemList)
        )
        SharePreference.setStringPref(
            requireActivity(),
            SharePreference.cartCount,
            cartItemList.size.toString()
        )
    }

    private fun cartQtyApi(
        cartqty: java.util.HashMap<String, String>,
        type: String
    ) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .cartQty(cartqty)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartQtyResponse = response.body.data
                    SharePreference.setStringPref(
                        requireActivity(),
                        SharePreference.cartCount,
                        response.body.count.toString()
                    )

                    when (response.body.status) {
                        1 -> {
                            /* when (type) {
                                 "increase" -> {
                                     cartList[position].qty = count
                                     val totalQty = cartList.sumOf { it.qty!! }
                                     _binding.tvTotalQtyCount.text = totalQty.toString()
                                     cartListAdapter.notifyDataSetChanged()

                                 }
                                 "remove" -> {
                                     cartList.removeAt(position)
                                     cartListAdapter.notifyDataSetChanged()
                                     if (cartList.size > 0) {
                                         _binding.rvShoppingCart.show()
                                         _binding.tvTotalQtyCount.show()
                                         _binding.clCoupon.show()
                                         _binding.tvNoDataFound.hide()
                                         _binding.clPaymentDetails.show()
                                     } else {
                                         _binding.rvShoppingCart.hide()
                                         _binding.tvTotalQtyCount.hide()
                                         _binding.clCoupon.hide()
                                         _binding.tvNoDataFound.show()
                                         _binding.clPaymentDetails.hide()
                                     }
                                     val totalQty = cartList.sumOf { it.qty!! }
                                     _binding.tvTotalQtyCount.text = totalQty.toString()
                                 }
                                 "decrease" -> {
                                     cartList[position].qty = count
                                     val totalQty = cartList.sumOf { it.qty!! }
                                     _binding.tvTotalQtyCount.text = totalQty.toString()
                                     cartListAdapter.notifyDataSetChanged()
                                 }
                             }
                             getCartList()*/
                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartQtyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartQtyResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(), response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }

    //adapter set Categorirs
    private fun categoriesAdapter(homeCategoriesList: ArrayList<HomeCategoriesItem>) {
        _binding.rvAllCategories.layoutManager = managerAllCategories
        allCategoriesAdapter =
            AllCateAdapter(requireActivity(), homeCategoriesList) { i: Int, s: String ->
                if (s == Constants.ItemClick) {
                    startActivity(
                        Intent(
                            requireActivity(),
                            ActCategoryProduct::class.java
                        ).putExtra("maincategory_id", homeCategoriesList[i].categoryId.toString())
                    )
                }
            }
        _binding.rvAllCategories.adapter = allCategoriesAdapter
    }

    // Categories api callig
    private fun callCategories() {
        Utils.showLoadingProgress(requireActivity())
        val categorieshashmap = HashMap<String, String>()
        categorieshashmap["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setHomeCategorys(currentPageCategorirs.toString(), categorieshashmap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val categoriesResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvAllCategories.show()
                                this@FragHome.currentPageCategorirs =
                                    categoriesResponse?.currentPage!!.toInt()
                                this@FragHome.total_pagesCategorirs =
                                    categoriesResponse.lastPage!!.toInt()
                                categoriesResponse.data?.let {
                                    homeCategoriesList.addAll(it)
                                }
                                /*if (currentPageCategorirs == response.body.data.lastPage) {
                                    _binding.btnShowMoreCategories.hide()
                                } else {
                                    _binding.btnShowMoreCategories.show()
                                }*/
                                if (currentPageCategorirs >= total_pagesCategorirs) {
                                    isLastPageCategorirs = true
                                }
                                isLoadingCategorirs = false
                            } else {
                                _binding.rvAllCategories.hide()
                            }
                            allCategoriesAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesResponse?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //Adapter set Best seller
    private fun bestsellerAdapter(bestsellersList: ArrayList<FeaturedProductsSub>) {
        _binding.rvBestsellers.layoutManager = managerBestsellers
        bestSellersAdapter =
            BestsellerAdapter(requireActivity(), bestsellersList) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            requireActivity(),
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (bestsellersList[i].inWhishlist == false) {
                            callWishlist(
                                "add", bestsellersList[i].id, "BestsellersList", i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                bestsellersList[i].id,
                                "BestsellersList",
                                i
                            )
                        }
                    } else {
                        openActivity(ActWelCome::class.java)
                        activity?.finish()
                    }
                } else if (s == Constants.CartClick) {
                    val data = bestsellersList[i]
                    if (!Utils.isLogin(requireActivity())) {
                        guestUserAddToCart(data, bestsellersList[i].id)
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                                .toString()
                        addtocart["product_id"] = bestsellersList[i].id.toString()
                        addtocart["variant_id"] = bestsellersList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = getString(R.string.theme_id)

                        addtocartApi(
                            addtocart,
                            bestsellersList[i].id,
                            bestsellersList[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(requireActivity(), ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, bestsellersList[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvBestsellers.adapter = bestSellersAdapter
    }

    private fun callBestseller() {
        Utils.showLoadingProgress(requireActivity())
        val bestseller = HashMap<String, String>()
        bestseller["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {

            val response =
                if (SharePreference.getStringPref(requireActivity(), SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(requireActivity())
                        .setBestSellerGuest(currentPageBestSeller.toString(), bestseller)
                } else {
                    ApiClient.getClient(requireActivity())
                        .setBestSeller(currentPageBestSeller.toString(), bestseller)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val bestsellerResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                if (Utils.isLogin(requireActivity())) {
                                    SharePreference.setStringPref(
                                        requireActivity(),
                                        SharePreference.cartCount,
                                        response.body.count.toString()
                                    )
                                    _binding.tvCount.text = response.body.count.toString()
                                }

                                _binding.rvBestsellers.show()
                                _binding.view.hide()
                                _binding.ivblack.show()
                                /* if (currentPageBestSeller == response.body.data?.lastPage) {
                                     _binding.btnShowMoreBestSellers.hide()
                                 } else {
                                     _binding.btnShowMoreBestSellers.show()
                                 }*/
                                this@FragHome.currentPageBestSeller =
                                    bestsellerResponse?.currentPage!!.toInt()
                                this@FragHome.total_pagesBestSeller =
                                    bestsellerResponse.lastPage!!.toInt()
                                bestsellerResponse.data?.let {
                                    bestsellersList.addAll(it)
                                }


                                if (currentPageBestSeller >= total_pagesBestSeller) {
                                    isLastPageBestSeller = true
                                }
                                isLoadingBestSeller = false
                            } else {
                                _binding.rvBestsellers.hide()
                                _binding.view.show()
                                _binding.ivblack.show()
                            }
                            bestSellersAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                bestsellerResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                bestsellerResponse?.data?.get(0)?.message.toString()
                            )
                            Utils.setInvalidToekn(requireActivity())
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        getString(R.string.something_went_wrong)
                    )
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()

        if(Utils.isCheckNetwork(requireActivity()))
        {
            lifecycleScope.launch {

                val currency = async {
                    callCurrencyApi()
                }

                currency.await()

                lifecycleScope.launch {
                    currentPage = 1
                    currentPageCategorirs = 1
                    currentPageBestSeller = 1
                    currentPageTrending = 1
                    featuredProductsList.clear()

                    callCurrencyApi()
                    callHeaderContentApi()
                    callFeaturedProduct()

                    featuredProductsSubList.clear()
                    categoriesProductsAdapter(featuredProductsSubList)
                    callCategorysProduct()

                    homeCategoriesList.clear()
                    categoriesAdapter(homeCategoriesList)
                    callCategories()

                    bestsellersList.clear()
                    bestsellerAdapter(bestsellersList)
                    callBestseller()


                    trendingProductsSubList.clear()
                    trandingAdapter(trendingProductsSubList)
                    callTrandingProduct()

                    Log.e(
                        "Count",
                        SharePreference.getStringPref(requireActivity(), SharePreference.cartCount)
                            .toString()
                    )
                    _binding.tvCount.text =
                        SharePreference.getStringPref(requireActivity(), SharePreference.cartCount)
                }
        }


        }else
        {
            _binding.view.show()
            Utils.errorAlert(
                requireActivity(),
                resources.getString(R.string.internet_connection_error)
            )
        }



    }

    private fun callTrandingProduct() {
        Utils.showLoadingProgress(requireActivity())
        val bestseller = HashMap<String, String>()
        bestseller["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {

            val response =
                if (SharePreference.getStringPref(requireActivity(), SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(requireActivity())
                        .setTrendingProductGuest(currentPageTrending.toString(), bestseller)
                } else {
                    ApiClient.getClient(requireActivity())
                        .setTrendingProduct(currentPageTrending.toString(), bestseller)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val trendingProductResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvTrandingProduct.show()
                                _binding.view.hide()

                                this@FragHome.currentPageTrending =
                                    trendingProductResponse?.currentPage!!.toInt()
                                this@FragHome.total_pagesTrending =
                                    trendingProductResponse.lastPage!!.toInt()
                                trendingProductResponse.data?.let {
                                    trendingProductsSubList.addAll(it)
                                }

                                if (currentPageTrending >= total_pagesTrending) {
                                    isLastPageTrending = true
                                }
                                isLoadingTrending = false
                            } else {
                                _binding.rvTrandingProduct.hide()
                                _binding.view.show()

                            }
                            trandingAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                trendingProductResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                trendingProductResponse?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun trandingAdapter(trendingProductsSubList: ArrayList<FeaturedProductsSub>) {
        _binding.rvTrandingProduct.layoutManager = managerTrandingProduct
        trandingAdapter =
            TrandingProductAdapter(
                requireActivity(),
                trendingProductsSubList
            ) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            requireActivity(),
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (trendingProductsSubList[i].inWhishlist == false) {
                            callWishlist(
                                "add", trendingProductsSubList[i].id, "TrendingList", i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                trendingProductsSubList[i].id,
                                "TrendingList",
                                i
                            )
                        }
                    } else {
                        openActivity(ActWelCome::class.java)
                        activity?.finish()
                    }
                } else if (s == Constants.CartClick) {
                    val data = trendingProductsSubList[i]
                    if (!Utils.isLogin(requireActivity())) {
                        guestUserAddToCart(data, trendingProductsSubList[i].id)
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                                .toString()
                        addtocart["product_id"] = trendingProductsSubList[i].id.toString()
                        addtocart["variant_id"] =
                            trendingProductsSubList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = getString(R.string.theme_id)

                        addtocartApi(
                            addtocart,
                            trendingProductsSubList[i].id,
                            trendingProductsSubList[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(requireActivity(), ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, trendingProductsSubList[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvTrandingProduct.adapter = trandingAdapter
    }


    private fun paginationTrending() {
        val paginationListener = object : PaginationScrollListener(managerTrandingProduct) {
            override fun isLastPage(): Boolean {
                return isLastPageTrending
            }

            override fun isLoading(): Boolean {
                return isLoadingTrending
            }

            override fun loadMoreItems() {
                isLoadingTrending = true
                currentPageTrending++
                callTrandingProduct()
            }
        }
        _binding.rvTrandingProduct.addOnScrollListener(paginationListener)
    }

    override fun onStop() {
        super.onStop()
        _binding.ivblack.hide()
        _binding.view.show()
    }
}