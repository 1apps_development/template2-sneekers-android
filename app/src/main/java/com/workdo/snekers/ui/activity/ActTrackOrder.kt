package com.workdo.snekers.ui.activity

import android.os.Build
import android.view.View
import androidx.annotation.RequiresApi
import com.workdo.snekers.R
import com.workdo.snekers.base.BaseActivity
import com.workdo.snekers.databinding.ActTrackOrderBinding

class ActTrackOrder : BaseActivity() {
    private lateinit var _binding: ActTrackOrderBinding
    var trackOrderStatus = ""

    override fun setLayout(): View = _binding.root

    @RequiresApi(Build.VERSION_CODES.M)
    override fun initView() {
        _binding = ActTrackOrderBinding.inflate(layoutInflater)
        trackOrderStatus = intent.getStringExtra("trackOrderStatus").toString()
        inti()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun inti() {
        _binding.ivBack.setOnClickListener { finish() }
        if (trackOrderStatus=="0"){
            _binding.ivOrderConfirm.setImageResource(R.drawable.ic_option)
            _binding.ivOrderDelivered.setImageResource(R.drawable.ic_unoption)
            _binding.view.setBackgroundColor(getColor(R.color.appcolor))
        }else if (trackOrderStatus=="1"){
            _binding.ivOrderConfirm.setImageResource(R.drawable.ic_option)
            _binding.ivOrderDelivered.setImageResource(R.drawable.ic_option)
            _binding.view.setBackgroundColor(getColor(R.color.appcolor))
        }
    }
}