package com.workdo.snekers.ui.option

import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.workdo.snekers.R
import com.workdo.snekers.base.BaseActivity
import com.workdo.snekers.databinding.ActCartBinding
import com.workdo.snekers.model.CategoriesModel
import com.workdo.snekers.ui.activity.ActShoppingCart

class ActCart : BaseActivity() {
    private lateinit var _binding: ActCartBinding
    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActCartBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.ivBack.setOnClickListener { finish() }

    }
}