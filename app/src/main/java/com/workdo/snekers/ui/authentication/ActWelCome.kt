package com.workdo.snekers.ui.authentication

import android.content.Intent
import android.view.View
import com.workdo.snekers.ui.authentication.ActLoginOption
import com.workdo.snekers.ui.authentication.ActRegisterOption
import com.workdo.snekers.base.BaseActivity
import com.workdo.snekers.databinding.ActWelComeBinding
import com.workdo.snekers.ui.activity.MainActivity

class ActWelCome : BaseActivity() {
    private lateinit var _binding: ActWelComeBinding

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActWelComeBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.btnLogin.setOnClickListener {
            openActivity(ActLoginOption::class.java)
        }
        _binding.btnSignup.setOnClickListener {
            openActivity(ActRegisterOption::class.java)
        }
        _binding.btnGuest.setOnClickListener {
            startActivity(
                Intent(
                    this@ActWelCome,
                    MainActivity::class.java
                )
            )
            finish()
        }
    }

}