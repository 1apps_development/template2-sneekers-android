package com.workdo.snekers.ui.activity

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.workdo.snekers.R
import com.workdo.snekers.adapter.SubCategoriesListAdapter
import com.workdo.snekers.adapter.ProductCategoryAdapter
import com.workdo.snekers.api.ApiClient
import com.workdo.snekers.base.BaseActivity
import com.workdo.snekers.databinding.ActCategoryProductBinding
import com.workdo.snekers.databinding.DlgConfirmBinding
import com.workdo.snekers.model.FeaturedProductsSub
import com.workdo.snekers.model.ProductListItem
import com.workdo.snekers.model.SubcategoryItem
import com.workdo.snekers.remote.NetworkResponse
import com.workdo.snekers.ui.authentication.ActWelCome
import com.workdo.snekers.utils.Constants
import com.workdo.snekers.utils.ExtensionFunctions.hide
import com.workdo.snekers.utils.ExtensionFunctions.show
import com.workdo.snekers.utils.PaginationScrollListener
import com.workdo.snekers.utils.SharePreference
import com.workdo.snekers.utils.Utils
import kotlinx.coroutines.launch
import java.lang.reflect.Type

class ActCategoryProduct : BaseActivity() {
    private lateinit var _binding: ActCategoryProductBinding
    private var categoriesList = ArrayList<SubcategoryItem>()
    private lateinit var categoriesAdapter: SubCategoriesListAdapter
    private var manager: GridLayoutManager? = null
    var subcategory_id: String = ""
    private var featuredProductsSubList = ArrayList<FeaturedProductsSub>()
    private var managerAllCategories: LinearLayoutManager? = null
    private lateinit var categoriesProductAdapter: ProductCategoryAdapter
    internal var isLoading = false
    internal var isLastPage = false
    private var currentPage = 1
    private var total_pages: Int = 0
    var countItem = ""
    var count = 1

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActCategoryProductBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.ivBack.setOnClickListener { finish() }
        manager = GridLayoutManager(this@ActCategoryProduct, 1, GridLayoutManager.HORIZONTAL, false)
        _binding.tvCount.text =
            SharePreference.getStringPref(this@ActCategoryProduct, SharePreference.cartCount)
                .toString()
        _binding.clcart.setOnClickListener { openActivity(ActShoppingCart::class.java) }
        managerAllCategories =
            GridLayoutManager(this@ActCategoryProduct, 2, GridLayoutManager.VERTICAL, false)
        categoriesProductsAdapter(featuredProductsSubList)
        nestedScrollViewPagination()
        productBannerApi()
    }

    //TODO Product Banner api
    private fun productBannerApi() {
        Utils.showLoadingProgress(this@ActCategoryProduct)
        val productBanner = HashMap<String, String>()
        productBanner["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActCategoryProduct)
                .productBanner(productBanner)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val bannersResponse = response.body.data?.themJson?.productsHeaderBanner
                    when (response.body.status) {
                        1 -> {
                            Glide.with(this@ActCategoryProduct).load(
                                ApiClient.ImageURL.BASE_URL.plus(
                                    bannersResponse?.productsHeaderBanner
                                )
                            )
                                .into(_binding.ivProductBanner)
                            _binding.tvmess.text =
                                bannersResponse?.productsHeaderBannerTitleText.toString()

                        }
                        0 -> {

                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActCategoryProduct)
                    } else {
                        Utils.errorAlert(
                            this@ActCategoryProduct,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActCategoryProduct,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActCategoryProduct,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun nestedScrollViewPagination() {
        _binding.rvFeaturedProduct.isNestedScrollingEnabled = false
        _binding.scrollView.viewTreeObserver?.addOnScrollChangedListener {
            val view = _binding.scrollView.getChildAt(_binding.scrollView.childCount - 1)
            val diff = view.bottom - (_binding.scrollView.height + _binding.scrollView.scrollY)
            if (diff == 0 && !isLoading && !isLastPage) {
                isLoading = true
                currentPage++
                callCategorysProduct()
            }
        }
    }

    //TODO Featured Product Api calling
    private fun callCategories() {
        Utils.showLoadingProgress(this@ActCategoryProduct)
        val request = HashMap<String, String>()
        request["maincategory_id"] = intent.getStringExtra("maincategory_id").toString()
        request["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            val response =
                if (SharePreference.getStringPref(this@ActCategoryProduct, SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(this@ActCategoryProduct).getSubCategoryListGuest(request)
                } else {
                    ApiClient.getClient(this@ActCategoryProduct).getSubCategoryList(request)
                }
            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val featuredProductResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            response.body.data?.subcategory?.let { categoriesList.addAll(it) }
                            if (categoriesList.size == 0) {
                                _binding.rvAllCategories.hide()
                            } else {
                                _binding.rvAllCategories.show()
                            }
                            subcategory_id = categoriesList[0].id.toString()
                            featuredProductsSubList.clear()
                            currentPage = 1
                            isLoading = false
                            isLastPage = false
                            callCategorysProduct()
                            categoriesAdapter.notifyDataSetChanged()
                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActCategoryProduct,
                                featuredProductResponse?.subcategory?.get(0)?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActCategoryProduct,
                                featuredProductResponse?.subcategory?.get(0)?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActCategoryProduct)
                    } else {
                        Utils.errorAlert(
                            this@ActCategoryProduct,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActCategoryProduct,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActCategoryProduct,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO Category list adapter
    private fun categoriesListAdapter(categoriesList: ArrayList<SubcategoryItem>) {
        _binding.rvAllCategories.layoutManager = manager
        categoriesAdapter =
            SubCategoriesListAdapter(this@ActCategoryProduct, categoriesList) { i: Int, s: String ->
                if (s == Constants.ItemClick) {
                    subcategory_id = categoriesList[i].id.toString()
                    featuredProductsSubList.clear()
                    currentPage = 1
                    total_pages = 0
                    isLastPage = false
                    isLoading = false
                    callCategorysProduct()
                }
            }
        _binding.rvAllCategories.adapter = categoriesAdapter
    }

    //TODO category product api calling
    private fun callCategorysProduct() {
        Utils.showLoadingProgress(this@ActCategoryProduct)
        val categoriesProduct = HashMap<String, String>()
        categoriesProduct["subcategory_id"] = subcategory_id
        categoriesProduct["maincategory_id"] = intent.getStringExtra("maincategory_id").toString()
        categoriesProduct["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            val response =
                if (SharePreference.getStringPref(this@ActCategoryProduct, SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(this@ActCategoryProduct)
                        .setCategorysProductGuest(currentPage.toString(), categoriesProduct)
                } else {
                    ApiClient.getClient(this@ActCategoryProduct)
                        .setCategorysProduct(currentPage.toString(), categoriesProduct)
                }
            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val categoriesProductResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                countItem = categoriesProductResponse?.data?.size.toString()
                                _binding.rvFeaturedProduct.show()
                                _binding.vieww.hide()
                                currentPage =
                                    categoriesProductResponse?.currentPage!!.toInt()
                                total_pages =
                                    categoriesProductResponse.lastPage!!.toInt()
                                categoriesProductResponse.data?.let {
                                    featuredProductsSubList.addAll(it)
                                }
                                Log.d("Current Page", currentPage.toString())
                                Log.d("total_pages", total_pages.toString())
                                if (currentPage >= total_pages) {
                                    isLastPage = true
                                }
                                isLoading = false
                            } else {
                                _binding.rvFeaturedProduct.hide()
                                _binding.vieww.show()
                            }
                            categoriesProductAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActCategoryProduct,
                                categoriesProductResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActCategoryProduct,
                                categoriesProductResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                    if (categoriesProductResponse?.to == null) {

                        _binding.tvFoundproduct.text =
                            getString(R.string.found).plus(" ").plus("0").plus(" ")
                                .plus(getString(R.string.product))
                    } else {
                        _binding.tvFoundproduct.text = getString(R.string.found).plus(" ").plus(
                            categoriesProductResponse.total
                        ).plus(" ").plus(getString(R.string.product))
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActCategoryProduct)
                    } else {
                        Utils.errorAlert(
                            this@ActCategoryProduct,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActCategoryProduct,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActCategoryProduct,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO Adapter set Categories Product
    private fun categoriesProductsAdapter(featuredProductsSubList: ArrayList<FeaturedProductsSub>) {
        _binding.rvFeaturedProduct.layoutManager = managerAllCategories
        countItem = featuredProductsSubList.size.toString()
        categoriesProductAdapter =
            ProductCategoryAdapter(
                this@ActCategoryProduct,
                featuredProductsSubList
            ) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            this@ActCategoryProduct,
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (featuredProductsSubList[i].inWhishlist == false) {
                            callWishlist(
                                "add", featuredProductsSubList[i].id, "CategoriesProduct", i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                featuredProductsSubList[i].id,
                                "CategoriesProduct",
                                i
                            )
                        }
                    } else {
                        openActivity(ActWelCome::class.java)
                        finish()
                    }
                } else if (s == Constants.CartClick) {
                    if (!Utils.isLogin(this@ActCategoryProduct)) {
                        guestUserAddToCart(
                            featuredProductsSubList[i],
                            featuredProductsSubList[i].id
                        )
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(
                                this@ActCategoryProduct,
                                SharePreference.userId
                            )
                                .toString()
                        addtocart["product_id"] = featuredProductsSubList[i].id.toString()
                        addtocart["variant_id"] =
                            featuredProductsSubList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = getString(R.string.theme_id)
                        addtocartApi(
                            addtocart,
                            featuredProductsSubList[i].id,
                            featuredProductsSubList[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(this@ActCategoryProduct, ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, featuredProductsSubList[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvFeaturedProduct.adapter = categoriesProductAdapter
    }

    //TODO guest user add to cart
    private fun guestUserAddToCart(data: FeaturedProductsSub, id: Int?) {

        val cartData = cartData(data)
        Log.e("cartData2", Gson().toJson(cartData))
        val cartListData = SharePreference.getStringPref(
            this@ActCategoryProduct,
            SharePreference.GuestCartList
        )
        val type: Type = object : TypeToken<ArrayList<ProductListItem>>() {}.type
        var cartList = Gson().fromJson<ArrayList<ProductListItem>>(cartListData, type)
        Log.e("cartListBefore2", Gson().toJson(cartList))
        if (cartList == null) {
            val cartDataList = ArrayList<ProductListItem>()
            cartDataList.add(cartData)
            SharePreference.setStringPref(
                this@ActCategoryProduct,
                SharePreference.GuestCartList,
                Gson().toJson(cartDataList)
            )
            SharePreference.setStringPref(
                this@ActCategoryProduct,
                SharePreference.cartCount,
                cartDataList.size.toString()
            )
            setCount(cartDataList.size)
        } else if (isProductAlreadyAdded(cartList, data.id.toString())) {
            for (i in 0 until cartList.size) {
                if (cartList[i].productId?.toInt() == id) {
                    val position = cartList[i]
                    Log.e("SelectId", position.toString())
                    dlgAlreadyCart(data.message, data.id, data.defaultVariantId, i)
                }
            }

        } else {
            dlgConfirm(data.name.plus(" ").plus("add successfully."))
            cartList.add(cartData)
            SharePreference.setStringPref(
                this@ActCategoryProduct,
                SharePreference.GuestCartList,
                Gson().toJson(cartList)
            )
            SharePreference.setStringPref(
                this@ActCategoryProduct,
                SharePreference.cartCount,
                cartList.size.toString()
            )

            setCount(cartList.size)
        }
    }

    //TODO cart data set
    private fun cartData(data: FeaturedProductsSub): ProductListItem {
        return ProductListItem(
            data.coverImagePath.toString(),
            data.defaultVariantId.toString(),
            data.finalPrice.toString(),
            data.discountPrice.toString(),
            data.id.toString(),
            1,
            data.name.toString(),
            "",
            "",
            data.variantName.toString(),
            data.originalPrice.toString()
        )
    }

    private fun isProductAlreadyAdded(cartList: ArrayList<ProductListItem>, id: String): Boolean {
        return cartList.filter { it.productId == id.toString() }.size == 1
    }

    //TODO add to cart api
    private fun addtocartApi(
        addtocart: HashMap<String, String>,
        id: Int?,
        defaultVariantId: Int?,
        i: Int?
    ) {
        Utils.showLoadingProgress(this@ActCategoryProduct)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActCategoryProduct)
                .addtocart(addtocart)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addtocart = response.body.data
                    when (response.body.status) {
                        1 -> {
                            dlgConfirm(response.body.data?.message)
                            setCount(addtocart?.count)
                            SharePreference.setStringPref(
                                this@ActCategoryProduct,
                                SharePreference.cartCount,
                                addtocart?.count.toString()
                            )
                        }
                        0 -> {
                            dlgAlreadyCart(response.body.data?.message, id, defaultVariantId, i)
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActCategoryProduct,
                                addtocart?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                            finish()
                            finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActCategoryProduct)
                    } else {
                        Utils.errorAlert(
                            this@ActCategoryProduct,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActCategoryProduct,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActCategoryProduct,
                        "Something went wrong"
                    )
                }
            }
        }
    }


    //TODO Checkout or Coutinue shopping dialog
    private fun dlgConfirm(message: String?) {
        val confirmDialogBinding = DlgConfirmBinding.inflate(layoutInflater)
        val dialog = Dialog(this@ActCategoryProduct)
        dialog.setContentView(confirmDialogBinding.root)
        confirmDialogBinding.tvProductDesc.text = message
        confirmDialogBinding.tvCheckOut.setOnClickListener {
            dialog.dismiss()
            openActivity(ActShoppingCart::class.java)
        }
        confirmDialogBinding.tvContinueShopping.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    //TODO Item alreay cart dialog
    private fun dlgAlreadyCart(message: String?, id: Int?, defaultVariantId: Int?, i: Int?) {
        val builder = AlertDialog.Builder(this@ActCategoryProduct)
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.product_already_again)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            val cartqty = java.util.HashMap<String, String>()

            if (Utils.isLogin(this@ActCategoryProduct)) {
                count = 1
                cartqty["product_id"] = id.toString()
                cartqty["user_id"] =
                    SharePreference.getStringPref(
                        this@ActCategoryProduct,
                        SharePreference.userId
                    ).toString()
                cartqty["variant_id"] = defaultVariantId.toString()
                cartqty["quantity_type"] = "increase"
                cartqty["theme_id"] = getString(R.string.theme_id)
                cartQtyApi(cartqty, "increase")
            } else {
                val cartListData = SharePreference.getStringPref(
                    this@ActCategoryProduct,
                    SharePreference.GuestCartList
                )
                val type: Type = object : TypeToken<java.util.ArrayList<ProductListItem>>() {}.type

                var cartList =
                    Gson().fromJson<java.util.ArrayList<ProductListItem>>(cartListData, type)
                var count = cartList[i!!].qty?.toInt()!!
                Log.e("Count", count.toString())
                cartList[i!!].qty = count.plus(1)

                manageOfflineData(cartList)
            }
        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            builder.setCancelable(true)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    //TODO item qty api
    private fun cartQtyApi(
        cartqty: java.util.HashMap<String, String>,
        type: String
    ) {
        Utils.showLoadingProgress(this@ActCategoryProduct)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActCategoryProduct)
                .cartQty(cartqty)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartQtyResponse = response.body.data
                    SharePreference.setStringPref(
                        this@ActCategoryProduct,
                        SharePreference.cartCount,
                        response.body.count.toString()
                    )

                    when (response.body.status) {
                        1 -> {
                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActCategoryProduct,
                                cartQtyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActCategoryProduct,
                                cartQtyResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActCategoryProduct)
                    } else {
                        Utils.errorAlert(
                            this@ActCategoryProduct, response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActCategoryProduct,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActCategoryProduct, "Something went wrong")
                }
            }
        }
    }

    //TODo Guest user offline data
    private fun manageOfflineData(cartItemList: java.util.ArrayList<ProductListItem>) {
        SharePreference.setStringPref(
            this@ActCategoryProduct,
            SharePreference.GuestCartList,
            Gson().toJson(cartItemList)
        )
        SharePreference.setStringPref(
            this@ActCategoryProduct,
            SharePreference.cartCount,
            cartItemList.size.toString()
        )
    }

    private fun setCount(count: Int?) {
        _binding.tvCount.text = count.toString()
    }

    //TODO wishlist api
    private fun callWishlist(type: String, id: Int?, itemType: String, position: Int) {
        Utils.showLoadingProgress(this@ActCategoryProduct)
        val wishlist = HashMap<String, String>()
        wishlist["user_id"] =
            SharePreference.getStringPref(this@ActCategoryProduct, SharePreference.userId)
                .toString()
        wishlist["product_id"] = id.toString()
        wishlist["wishlist_type"] = type
        wishlist["theme_id"] =getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActCategoryProduct)
                .setWishlist(wishlist)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val wishlistResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if (type == "add") {
                                if (itemType == "CategoriesProduct") {
                                    featuredProductsSubList[position].inWhishlist = true
                                    categoriesProductAdapter.notifyItemChanged(position)
                                }
                            } else {
                                if (itemType == "CategoriesProduct") {
                                    featuredProductsSubList[position].inWhishlist = false
                                    categoriesProductAdapter.notifyItemChanged(position)
                                }
                            }
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActCategoryProduct,
                                wishlistResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActCategoryProduct,
                                wishlistResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActCategoryProduct)
                    } else {
                        Utils.errorAlert(
                            this@ActCategoryProduct,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActCategoryProduct,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActCategoryProduct,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        categoriesList.clear()
        categoriesListAdapter(categoriesList)
        callCategories()

        setCount(
            SharePreference.getStringPref(this@ActCategoryProduct, SharePreference.cartCount)
                ?.toInt()
        )

    }
}