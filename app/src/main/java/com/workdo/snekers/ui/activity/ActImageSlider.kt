package com.workdo.snekers.ui.activity

import android.util.Log
import android.view.View
import com.denzcoskun.imageslider.models.SlideModel
import com.workdo.snekers.api.ApiClient
import com.workdo.snekers.base.BaseActivity
import com.workdo.snekers.databinding.ActImageSliderBinding
import com.workdo.snekers.model.ProductImageItem

class ActImageSlider : BaseActivity() {
    private lateinit var imageSliderBinding: ActImageSliderBinding
    var imgList: ArrayList<ProductImageItem>? = null

    override fun setLayout(): View = imageSliderBinding.root

    override fun initView() {
        imageSliderBinding = ActImageSliderBinding.inflate(layoutInflater)
        imgList = intent.getParcelableArrayListExtra("imageList")
        Log.e("ImageList",imgList.toString())
        val imageList = ArrayList<SlideModel>()
        for (i in 0 until imgList?.size!!) {
            val slideModel = SlideModel(ApiClient.ImageURL.BASE_URL.plus(imgList!![i].imagePath))
            imageList.add(slideModel)
        }
        imageSliderBinding.imageSlider.setImageList(imageList)
        imageSliderBinding.ivCancle.setOnClickListener {
            finish()
        }
    }
}