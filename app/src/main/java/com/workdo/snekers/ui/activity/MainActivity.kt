package com.workdo.snekers.ui.activity

import android.app.AlertDialog
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.workdo.snekers.R
import com.workdo.snekers.base.BaseActivity
import com.workdo.snekers.databinding.ActivityMainBinding
import com.workdo.snekers.ui.authentication.ActWelCome
import com.workdo.snekers.ui.fragment.*
import com.workdo.snekers.utils.SharePreference
import com.workdo.snekers.utils.Utils

class MainActivity : BaseActivity() {
    private lateinit var _binding: ActivityMainBinding
    var pos = ""
    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActivityMainBinding.inflate(layoutInflater)

        val name = SharePreference.getStringPref(this@MainActivity, SharePreference.userName)
        Log.e("name", name.toString())
        pos = intent.getStringExtra("pos").toString()
        Log.e("Pos", pos)
        if(SharePreference.getStringPref(this@MainActivity,SharePreference.cartCount).isNullOrEmpty())
        {
            SharePreference.setStringPref(this@MainActivity,SharePreference.cartCount,"0")
        }

        when (pos) {
            "1" -> {
                setCurrentFragment(FragHome())
                _binding.bottomNavigation.selectedItemId = R.id.ivBestSellers
            }
            "2" -> {
                setCurrentFragment(FragProduct())
                _binding.bottomNavigation.selectedItemId = R.id.ivProduct
            }
            "3" -> {
                setCurrentFragment(FragAllCategories())
                _binding.bottomNavigation.selectedItemId = R.id.ivCategories
            }
            "4" -> {
                setCurrentFragment(FragWishList())
                _binding.bottomNavigation.selectedItemId = R.id.ivWishList
            }
            "5" -> {
                setCurrentFragment(FragSetting())
                _binding.bottomNavigation.selectedItemId = R.id.ivSettings
            }
            else -> {
                setCurrentFragment(FragHome())
            }
        }
        bottomSheetItemNavigation()
    }

    private fun bottomSheetItemNavigation() {
        _binding.bottomNavigation.setOnItemSelectedListener { item ->
            val fragment = supportFragmentManager.findFragmentById(R.id.mainContainer)

            when (item.itemId) {
                R.id.ivBestSellers -> {
                    if (fragment !is FragHome) {
                        supportFragmentManager.fragments.clear()
                        setCurrentFragment(FragHome())
                    }
                    true
                }
                R.id.ivProduct -> {
                    if (fragment !is FragProduct) {
                        supportFragmentManager.fragments.clear()
                        setCurrentFragment(FragProduct())
                    }
                    true
                }
                R.id.ivCategories -> {

                    if (fragment !is FragAllCategories) {
                        supportFragmentManager.fragments.clear()
                        setCurrentFragment(FragAllCategories())
                        SharePreference.setStringPref(this@MainActivity,SharePreference.clickMenu,"click")
                    }else
                    {
                        fragment.showLayout()
                    }
                    true

                }
                R.id.ivWishList -> {
                    if (Utils.isLogin(this@MainActivity)) {
                        if (fragment !is FragWishList) {
                            supportFragmentManager.fragments.clear()
                            setCurrentFragment(FragWishList())
                        }
                    } else {

                        Utils.openWelcomeScreen(this@MainActivity)

                    }

                    true
                }
                R.id.ivSettings -> {
                    if (Utils.isLogin(this@MainActivity)) {
                        if (fragment !is FragSetting) {
                            supportFragmentManager.fragments.clear()
                            setCurrentFragment(FragSetting())
                        }
                    } else {
                        Utils.openWelcomeScreen(this@MainActivity)

                    }


                    true
                }


                else -> {
                    false
                }
            }
        }
    }


    private fun setCurrentFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.mainContainer, fragment)
            commit()
        }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            finishAffinity()
        } else {
            mExitDialog()
        }
    }

    private fun mExitDialog() {
        val builder = AlertDialog.Builder(this@MainActivity)
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.exit)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            dialogInterface.dismiss()
            builder.setCancelable(true)
            ActivityCompat.finishAfterTransition(this@MainActivity)
            ActivityCompat.finishAffinity(this@MainActivity);
            finish()
        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            dialogInterface.dismiss()
            builder.setCancelable(true)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }
}