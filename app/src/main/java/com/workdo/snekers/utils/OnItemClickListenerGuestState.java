package com.workdo.snekers.utils;

import com.workdo.snekers.model.StateListData;

public interface OnItemClickListenerGuestState {
    void onItemClickStateGuest(StateListData item);
}
