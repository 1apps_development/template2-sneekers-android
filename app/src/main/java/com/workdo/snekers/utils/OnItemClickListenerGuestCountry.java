package com.workdo.snekers.utils;

import com.workdo.snekers.model.CountryDataItem;

public interface OnItemClickListenerGuestCountry {
    void onItemClickGuest(CountryDataItem item);
}
