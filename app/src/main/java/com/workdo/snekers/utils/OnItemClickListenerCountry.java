package com.workdo.snekers.utils;

import com.workdo.snekers.model.CountryDataItem;

public interface OnItemClickListenerCountry {
    void onItemClick(CountryDataItem item);
}
