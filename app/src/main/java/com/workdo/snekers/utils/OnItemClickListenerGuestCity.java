package com.workdo.snekers.utils;

import com.workdo.snekers.model.CityListData;

public interface OnItemClickListenerGuestCity {
    void onItemClickCityGuest(CityListData item);
}
