package com.workdo.snekers.utils;

import com.workdo.snekers.model.StateListData;

public interface OnItemClickListenerState {
    void onItemClickState(StateListData item);
}
