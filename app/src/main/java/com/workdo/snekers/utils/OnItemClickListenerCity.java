package com.workdo.snekers.utils;

import com.workdo.snekers.model.CityListData;

public interface OnItemClickListenerCity {
    void onItemClickCity(CityListData item);
}
