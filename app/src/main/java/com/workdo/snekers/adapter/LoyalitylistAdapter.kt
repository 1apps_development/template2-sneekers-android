package com.workdo.snekers.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.snekers.R
import com.workdo.snekers.databinding.CellLoyalityBinding
import com.workdo.snekers.model.OrderListData
import com.workdo.snekers.utils.SharePreference
import com.workdo.snekers.utils.Utils

class LoyalitylistAdapter(
    private val context: Activity,
    private val orderlist: ArrayList<OrderListData>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<LoyalitylistAdapter.WishlistViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency_name)

    inner class WishlistViewHolder(private val binding: CellLoyalityBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: OrderListData,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            binding.tvDate.text =
                context.getString(R.string.date).plus(" ").plus(Utils.getDate(data.date.toString()))
            binding.tvOrderId.text = "#".plus(data.productOrderId)
            binding.tvFeaturedProductPrice.text ="+".plus(" ").plus(Utils.getPrice(data.rewardPoints.toString())).plus(" ").plus(
                currency)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WishlistViewHolder {
        val view =
            CellLoyalityBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return WishlistViewHolder(view)
    }

    override fun onBindViewHolder(holder: WishlistViewHolder, position: Int) {
        holder.bind(orderlist[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {
        return orderlist.size
    }
}