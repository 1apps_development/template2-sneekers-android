package com.workdo.snekers.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.snekers.api.ApiClient
import com.workdo.snekers.databinding.CellAllCategoriesBinding
import com.workdo.snekers.utils.Constants
import com.workdo.snekers.model.HomeCategoriesItem

class AllCateAdapter(
    private val context: Activity,
    private val categoryList: ArrayList<HomeCategoriesItem>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<AllCateAdapter.AllCateViewHolder>() {

    inner class AllCateViewHolder(private val binding: CellAllCategoriesBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: HomeCategoriesItem,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {

            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.image))
                .into(binding.allcategoriesimage)
            binding.tvCategoriesType.text = data.name.toString()
            binding.tvCount.text = "Featured"
            itemView.setOnClickListener {
                itemClick(position, Constants.ItemClick)
            }
            binding.tvshowmore.setOnClickListener {
                itemClick(position, Constants.ItemClick)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AllCateViewHolder {
        val view =
            CellAllCategoriesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AllCateViewHolder(view)
    }

    override fun onBindViewHolder(holder: AllCateViewHolder, position: Int) {
        holder.bind(categoryList[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {
        return categoryList.size
    }
}