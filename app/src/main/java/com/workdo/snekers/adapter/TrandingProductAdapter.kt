package com.workdo.snekers.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.snekers.R
import com.workdo.snekers.api.ApiClient
import com.workdo.snekers.databinding.CellFeaturedProductsBinding
import com.workdo.snekers.databinding.CellProductsHomeBinding
import com.workdo.snekers.model.FeaturedProductsSub
import com.workdo.snekers.utils.Constants
import com.workdo.snekers.utils.ExtensionFunctions.hide
import com.workdo.snekers.utils.ExtensionFunctions.show
import com.workdo.snekers.utils.SharePreference
import com.workdo.snekers.utils.Utils

class TrandingProductAdapter(
    private val context: Activity,
    private val providerList: ArrayList<FeaturedProductsSub>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<TrandingProductAdapter.TrandingProductViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency_name).toString()

    inner class TrandingProductViewHolder(private val binding: CellProductsHomeBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: FeaturedProductsSub,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            Log.e("Cuurent", currency)
            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.coverImagePath))
                .into(binding.ivfeaturedProduct)
            binding.tvFeaturedProductName.text = data.name.toString()
            binding.tvFeaturedProductPrice.text =
                Utils.getPrice(data.finalPrice.toString())
            binding.tvProductPrice.text =
                Utils.getPrice(data.originalPrice.toString()).plus(" ").plus(currency)
            binding.tvcurrenytype.text = currency

            binding.tvTag.text = data.tagApi
            if (data.inWhishlist== true) {
                binding.ivWishlist.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.ic_wishlist, null)
            } else {
                binding.ivWishlist.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.ic_diswishlist, null)
            }

            if(Utils.isLogin(context))
            {
                binding.ivWishlist.show()
            }else
            {
                binding.ivWishlist.hide()

            }
            binding.ivWishlist.setOnClickListener {
                itemClick(position, Constants.FavClick)
            }
            binding.ivAddtocard.setOnClickListener {
                itemClick(position, Constants.CartClick)
            }
            itemView.setOnClickListener {
                itemClick(position, Constants.ItemClick)
            }


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrandingProductViewHolder {
        val view =
            CellProductsHomeBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return TrandingProductViewHolder(view)
    }

    override fun onBindViewHolder(holder: TrandingProductViewHolder, position: Int) {
        holder.bind(providerList[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return providerList.size
    }
}