package com.workdo.snekers.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.snekers.R
import com.workdo.snekers.api.ApiClient
import com.workdo.snekers.model.SubCategoryItems
import com.workdo.snekers.ui.activity.ActMenuCatProduct

class MenuSubListAdapter(
    var context: Activity,
    private val mList: List<SubCategoryItems>,
) : RecyclerView.Adapter<MenuSubListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_menusublist, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val categoriesModel = mList[position]
        holder.tvCategoriesname.text = categoriesModel.name
        Glide.with(context)
            .load(ApiClient.ImageURL.BASE_URL.plus(categoriesModel.iconImgPath)).into(holder.ivcategories)

        holder.itemView.setOnClickListener {
            val intent = Intent(holder.itemView.context, ActMenuCatProduct::class.java)
            intent.putExtra("main_id", categoriesModel.maincategoryId.toString())
            intent.putExtra(
                "sub_id",
                categoriesModel.subcategoryId.toString()
            )
            intent.putExtra("name",categoriesModel.name.toString())
            intent.putExtra(
                "menu",
                "menu"
            )
            holder.itemView.context.startActivity(intent)
            context.finish()

        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val tvCategoriesname: TextView = itemView.findViewById(R.id.tvCategoriesname)
        val ivcategories: ImageView = itemView.findViewById(R.id.ivcategories)
    }


}