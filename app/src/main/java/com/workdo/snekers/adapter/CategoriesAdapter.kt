package com.workdo.snekers.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.workdo.snekers.R
import com.workdo.snekers.model.FeaturedProducts

class CategoriesAdapter(
    var context: Activity,
    private val featuredList: List<FeaturedProducts>,
    private val onClick: (String, String, String) -> Unit
) : RecyclerView.Adapter<CategoriesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_categories, parent, false)

        return ViewHolder(view)
    }

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val categoriesModel = featuredList[position]
        if (categoriesModel.isSelect) {
            holder.card.background =
                ResourcesCompat.getDrawable(context.resources, R.drawable.bgappcolor_18, null)
            holder.textView.setTextColor(ContextCompat.getColor(context, R.color.white))
        } else {
            holder.card.background =
                ResourcesCompat.getDrawable(context.resources, R.drawable.bglightpinkr_18, null)
            holder.textView.setTextColor(ContextCompat.getColor(context, R.color.white))
        }
        holder.textView.text = categoriesModel.name
        holder.itemView.setOnClickListener {
            for (element in featuredList) {
                element.isSelect = false
            }
            categoriesModel.isSelect = true
            notifyDataSetChanged()

            onClick(
                categoriesModel.id.toString(),
                categoriesModel.name.toString(),
                categoriesModel.maincategoryId.toString()
            )
        }
    }

    override fun getItemCount(): Int {
        return featuredList.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val textView: TextView = itemView.findViewById(R.id.tvCategoriesname)
        val card: ConstraintLayout = itemView.findViewById(R.id.cl)
    }

    private fun onClick(id: String, name: String, mainId: String) {
        onClick.invoke(id, name, mainId)
    }
}