package com.workdo.snekers.adapter

import android.app.Activity
import android.content.res.ColorStateList
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.snekers.R
import com.workdo.snekers.api.ApiClient
import com.workdo.snekers.utils.Constants
import com.workdo.snekers.databinding.CellProductlistingBinding
import com.workdo.snekers.model.SubcategoryItem

class SubCategoriesListAdapter(
    private val context: Activity,
    private val addressList: ArrayList<SubcategoryItem>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<SubCategoriesListAdapter.AddressViewHolder>() {
    var firsttime = 0

    inner class AddressViewHolder(private val binding: CellProductlistingBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: SubcategoryItem,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {

            if (firsttime == 0) {
                for (i in 0 until addressList.size) {
                    addressList[0].isSelect = true
                }
            } else {

            }
            if (data.isSelect) {
                binding.card.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.bgappcolor_10, null)
                binding.tvProductName.setTextColor(ContextCompat.getColor(context, R.color.white))
                binding.ivCart.imageTintList =
                    ColorStateList.valueOf(
                        ResourcesCompat.getColor(
                            context.resources,
                            R.color.white,
                            null
                        )
                    )
            } else {
                binding.card.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.bglightpinkr_10, null)
                binding.tvProductName.setTextColor(ContextCompat.getColor(context, R.color.black))
                binding.ivCart.imageTintList =
                    ColorStateList.valueOf(
                        ResourcesCompat.getColor(
                            context.resources,
                            R.color.darkgray,
                            null
                        )
                    )
            }

            binding.tvProductName.text = data.name
            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.iconPath)).into(binding.ivCart)
            itemView.setOnClickListener {
                firsttime = 1
                Log.e("Postion", position.toString())
                addressList[0].isSelect = false
                data.isFirstPositio = false
                for (element in addressList) {
                    element.isSelect = false

                }
                data.isSelect = true
                notifyDataSetChanged()


                itemClick(position, Constants.ItemClick)
            }


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressViewHolder {
        val view =
            CellProductlistingBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AddressViewHolder(view)
    }

    override fun onBindViewHolder(holder: AddressViewHolder, position: Int) {
        holder.bind(addressList[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return addressList.size
    }
}