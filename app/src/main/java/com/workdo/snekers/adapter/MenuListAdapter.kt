package com.workdo.snekers.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.workdo.snekers.R
import com.workdo.snekers.model.DataItems

class MenuListAdapter(
    var context: Activity,
    private val mList: List<DataItems>,
    private val onFilterClick: (Int, String) -> Unit
) : RecyclerView.Adapter<MenuListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_menulist, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val categoriesModel = mList[position]
        holder.tvProductName.text = categoriesModel.name
        val isVisible: Boolean = categoriesModel.expand

        holder.rvSubProductList.visibility = if (isVisible) View.VISIBLE else View.GONE
        holder.itemView.setOnClickListener {
            if (categoriesModel.subCategory.isNullOrEmpty()) {
                holder.rvSubProductList.isClickable = false
            } else {
                categoriesModel.expand = !categoriesModel.expand
                Log.d("isVisible", isVisible.toString())
                notifyItemChanged(position)
            }
        }
        val adapter: MenuSubListAdapter? = categoriesModel.subCategory?.let {
            MenuSubListAdapter(context,
                it
            )
        }
        holder.rvSubProductList.adapter = adapter
        holder.rvSubProductList.layoutManager =
            LinearLayoutManager(holder.rvSubProductList.context, LinearLayoutManager.VERTICAL, false)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val tvProductName: TextView = itemView.findViewById(R.id.tvProductName)
        val rvSubProductList: RecyclerView = itemView.findViewById(R.id.rvSubProductList)
    }

    private fun onFilterClick(id: Int, name: String) {
        onFilterClick.invoke(id, name)
    }
}